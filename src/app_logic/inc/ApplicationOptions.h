#ifndef DND_UTILITIES_INITIATIVE_TRACKER_CMD_OPTIONS_H
#define DND_UTILITIES_INITIATIVE_TRACKER_CMD_OPTIONS_H

#include <string>
#include <ostream>

namespace CheesySoftware
{
  namespace InitiativeTracker
  {
    enum class Verbosity
    {
      NO_INFO,       //0
      ERROR,         //1
      WARNING,       //2
      INFO,          //3
      VERBOSE_INFO,  //4
      DEBUG_INFO,    //5
      FUNCTION_NAME, //6
      ALL_INFO       //7
    };

    struct AppOptions
    {
      Verbosity verbosity = Verbosity::INFO;
      std::string enemyFilePath = "";
      std::string playerFilePath = "";
      std::string encounterFilePath = "";
      std::string logPath = "./";
    };
  }
}

std::ostream& operator<<(std::ostream& out, const CheesySoftware::InitiativeTracker::Verbosity& toPrint);

#endif
