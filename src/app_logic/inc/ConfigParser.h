#ifndef DND_UTILITIES_INITIATIVE_TRACKER_CONFIG_PARSER_H
#define DND_UTILITIES_INITIATIVE_TRACKER_CONFIG_PARSER_H

#include <memory>
#include <vector>
#include <utility>
#include <string>

#include "json.hpp"

#include "DiceRoller.h"
#include "OutputManager.h"
#include "ApplicationOptions.h"

//TODO figure out a good way to get output manager working with config parser

namespace CheesySoftware
{
  namespace InitiativeTracker
  {
    struct Creature
    {
      std::string name;
      bool advantage;
      DiceRoller roller = DiceRoller(1, 20);
    };
    struct Player
    {
      std::string playerName;
      std::string characterName;
      inline std::string toString() const
      {
        return characterName + "(" + playerName + ")";
      }
    };
    std::ostream& operator<<(std::ostream& out, Player toPrint);

    class ConfigParser
    {
      private:
        //private functions
        static std::vector<Creature> parseCreature(nlohmann::json creatureStruct);

      public:
        //static member data
        static std::shared_ptr<cheeto::OutputManager> m_log;

        //public functions
        static std::vector<Creature> parseEnemyFile(const std::string& filePath);
        static std::vector<Creature> parseEnemyJson(const nlohmann::json& enemyJson);
        static std::vector<Player> parsePlayerFile(const std::string& filePath);
        static std::vector<Player> parsePlayerJson(const nlohmann::json& playerJson);
        static std::pair<std::vector<Creature>, std::vector<Player>> parseEncounterFile(const std::string& filePath);
        static std::pair<std::vector<Creature>, std::vector<Player>> parseEncounterJson(const nlohmann::json& encounterJson);
    };
  }
}

#endif
