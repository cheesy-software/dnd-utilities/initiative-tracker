#ifndef CHEESY_SOFTWARE_INITIATIVE_TRACKER_H
#define CHEESY_SOFTWARE_INITIATIVE_TRACKER_H

#include <memory>
#include <functional>
#include <string>
#include <tuple>
#include <vector>
#include <unordered_map>

#include "ApplicationOptions.h"
#include "OutputManager.h"
#include "ConfigParser.h"

namespace CheesySoftware
{
  namespace InitiativeTracker
  {
    struct Combatant
    {
      long roll;
      std::string name;
      bool player;
      bool dead;
      bool operator<(const Combatant& toCompare) const
      {
        return this->roll < toCompare.roll;
      }
      bool operator>(const Combatant& toCompare) const
      {
        return this->roll > toCompare.roll;
      }
    };
    class InitiativeTracker
    {
    public:
      //types
      /**
        * Used to sort combatants that have tied on the initiative roll
        * Will be called when startCombat results in combatants having tied with their rolls.
        * May be called multiple times in a row depending on how many ties occured.
        * Functions used for this must make the following garuntees
        *   * data stored in the combatants passed in as a paramater must not be changed when
        *     being put into the sorted vector, simply rearanged
        * @param 1: Combatants that have tied
        * @return: vector of the combatants that have been sorted, first in the vector will go first in combat
        */
      using TieBreakerFunc_t = std::function<std::vector<Combatant>(const std::vector<Combatant>&)>;
      /**
        * Used to request player rolls one at a time
        * Will be called when startCombat is called, once for each player
        * @param 1: player for which the roll is being requested
        * @return: int that is the player's initiative roll
        */
      using PlayerRollFunc_t = std::function<int(const Player&)>;
      /**
        * Used to request all player rolls at once
        * Will be called when startCombat is called once
        * Functions used for this must make the following garuntees
        *   * data in the players passed into the function will not be modified when being
        *     put into the return vector, simply associated with the appropriate roll
        * @param 1: vector of the players in the fight
        * @return: vector of pairs where the first element is the player and the second element in their roll
        */
      using MultiPlayerRollFunc_t = std::function<std::vector<std::pair<Player, int>>(const std::vector<Player>&)>;

    private:
      //member variables
      std::shared_ptr<cheeto::OutputManager> m_log;
      std::unique_ptr<AppOptions> m_options;
      TieBreakerFunc_t m_tieBreakerFunc;
      PlayerRollFunc_t m_playerRollFunc;
      MultiPlayerRollFunc_t m_multiPlayerRollFunc;

      std::uint64_t m_currentTurn;
      std::uint64_t m_currentRound;
      std::unordered_map<std::string, Creature> m_creatures; //[name] ->creature
      std::unordered_map<std::string, Player> m_players; //[playerName] -> player
      std::vector<Combatant> m_turnOrder;
      bool m_inCombat;

      //private constructor with all params
      InitiativeTracker(const AppOptions& options,
                        const TieBreakerFunc_t& tieBreakerFunc,
                        const PlayerRollFunc_t& playerRollFunc,
                        const MultiPlayerRollFunc_t& multiPlayerRollFunc,
                        std::shared_ptr<cheeto::OutputManager> out);

      //private functions
      void configFromSettings();
      void addEnemies(const std::vector<Creature>& enemies);
      void addPlayers(const std::vector<Player>& players);

    public:
      //constructors
      InitiativeTracker(const AppOptions& options,
                        const TieBreakerFunc_t& tieBreakerFunc,
                        const PlayerRollFunc_t& playerRollFunc,
                        std::shared_ptr<cheeto::OutputManager> out);
      InitiativeTracker(const AppOptions& options,
                        const TieBreakerFunc_t& tieBreakerFunc,
                        const MultiPlayerRollFunc_t& multiPlayerRollFunc,
                        std::shared_ptr<cheeto::OutputManager> out);

      //combat functions
      void startCombat();
      void stopCombat();
      void nextTurn();
      Combatant currentCombatant() const;
      Player onDeckPlayer() const;

      //combatant functions
      void killEnemy(const std::string& name);
      void reviveEnemy(const std::string& name);
      void killPlayer(const std::string& name);
      void revivePlayer(const std::string& name);

      //setup functions
      bool addPlayersFromFile(const std::string& filePath);
      bool addEnemiesFromFile(const std::string& filePath);
      bool addEncounterFromFile(const std::string& filePath);

      //getters
      bool getInCombat() const;
      std::uint64_t getRound() const;
      std::uint64_t getTurn() const;
      Player getPlayer(const std::string& name) const;
      bool playerExists(const std::string& name) const;
      Creature getEnemy(const std::string& name) const;
      bool enemyExists(const std::string& name) const;
      std::vector<Combatant> getTurnOrder() const;
    };
  }
}

#endif
