#include "ApplicationOptions.h"

using CheesySoftware::InitiativeTracker::Verbosity;

//output stream operator for verbosity
std::ostream& operator<<(std::ostream& out, const Verbosity& toPrint)
{
  switch (toPrint)
  {
    case Verbosity::ALL_INFO:
      out << "ALL_INFO";
      break;
    case Verbosity::DEBUG_INFO:
      out << "DEBUG_INFO";
      break;
    case Verbosity::ERROR:
      out << "ERROR";
      break;
    case Verbosity::FUNCTION_NAME:
      out << "FUNCTION_NAME";
      break;
    case Verbosity::INFO:
      out << "INFO";
      break;
    case Verbosity::NO_INFO:
      out << "NO_INFO";
      break;
    case Verbosity::VERBOSE_INFO:
      out << "VERBOSE_INFO";
      break;
    case Verbosity::WARNING:
      out << "WARNING";
      break;
  } //end  switch (toPrint)
  return out;
}
