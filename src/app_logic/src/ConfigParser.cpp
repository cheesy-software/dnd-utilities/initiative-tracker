#include <fstream>
#include <memory>

#include "ConfigParser.h"
#include "OutputManager.h"

namespace CheesySoftware
{
  namespace InitiativeTracker
  {
    void to_json(nlohmann::json& j, const Player& c)
    {
      j = nlohmann::json{
        {"PlayerName", c.playerName},
        {"CharacterName", c.characterName}
      };
    }
    void from_json(const nlohmann::json& j, Player& c)
    {
      j.at("PlayerName").get_to(c.playerName);
      j.at("CharacterName").get_to(c.characterName);
    }

    std::ostream& operator<<(std::ostream& out, Player toPrint)
    {
      out << toPrint.toString();
      return out;
    }

    //member data
    std::shared_ptr<cheeto::OutputManager> ConfigParser::m_log = nullptr;

    //private functions
    std::vector<Creature> ConfigParser::parseCreature(nlohmann::json creatureStruct)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parseCreature\n";
      }
      //TODO make safer
      //TODO implement to_ and from_json functions
      Creature creature;
      creature.roller.setModifier(creatureStruct["Bonus"]);
      creature.advantage = creatureStruct["Advantage"];
      creature.name = creatureStruct["Name"];
      if (m_log)
      {
        (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: ConfigParser: adding " << creature.name << "\n";
      }

      //create number of creatures equal to quantity
      std::vector<Creature> creatures;
      for (int i = 0; i < creatureStruct["Quantity"]; i++)
      {
        if (m_log)
        {
          (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: ConfigParser: adding copy " << i+1 << "\n";
        }
        Creature toAdd = creature;
        toAdd.name += std::string("_");
        toAdd.name += std::to_string(i + 1);
        creatures.push_back(toAdd);
      } //end  for (int i = 0; i < creatureStruct["Quantity"]; i++)
      if (m_log)
      {
        (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: ConfigParser: added " << creatures.size() << " enemies\n";
      }

      return creatures;
    }

    //public functions
    std::vector<Creature> ConfigParser::parseEnemyFile(const std::string& filePath)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parseEnemyFile\n";
      }
      //parse json file into a json object
      nlohmann::json j;
      std::ifstream f(filePath);
      f >> j;

      //parse the json object
      return parseEnemyJson(j["Enemies"]);
    }
    std::vector<Creature> ConfigParser::parseEnemyJson(const nlohmann::json& enemyJson)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parseEnemyJson\n";
      }
      std::vector<Creature> toReturn;

      //iterate over the creatures
      for (auto creature : enemyJson)
      {
        if (m_log)
        {
          (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: ConfigParser: adding new creature\n";
        }
        auto parsedCreatures = parseCreature(creature);
        toReturn.insert(toReturn.end(), parsedCreatures.begin(), parsedCreatures.end());
      } //end  for (auto creature : enemyJson["Enemies"])

      return toReturn;
    }
    std::vector<Player> ConfigParser::parsePlayerFile(const std::string& filePath)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parsePlayerFile\n";
      }
      //parse json file
      nlohmann::json j;
      std::ifstream f(filePath);
      f >> j;

      return parsePlayerJson(j["Players"]);
    }
    std::vector<Player> ConfigParser::parsePlayerJson(const nlohmann::json& playerJson)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parsePlayerJson\n";
      }
      std::vector<Player> players;

      //read in players from file
      for (auto& player : playerJson)
      {
        if (m_log)
        {
          (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: ConfigParser: adding new player\n";
        }
        players.push_back(player.get<Player>());
      } //end  for (auto& player : j["Players"])

      return players;
    }
    std::pair<std::vector<Creature>, std::vector<Player>> ConfigParser::parseEncounterFile(const std::string& filePath)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parseEncounterFile\n";
      }
      //parse json file
      nlohmann::json j;
      std::ifstream f(filePath);
      f >> j;

      return parseEncounterJson(j);
    }
    std::pair<std::vector<Creature>, std::vector<Player>> ConfigParser::parseEncounterJson(const nlohmann::json& encounterJson)
    {
      if (m_log)
      {
        (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: ConfigParser: parseEncounterJson\n";
      }
      auto enemies = parseEnemyJson(encounterJson["Enemies"]);
      auto players = parsePlayerJson(encounterJson["Players"]);

      return {enemies, players};
    }
  }
}

