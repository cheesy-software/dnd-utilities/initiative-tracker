#include <algorithm>
#include <iostream>

#include "InitiativeTracker.h"

#include "ConfigParser.h"

namespace CheesySoftware
{
  namespace InitiativeTracker
  {

    //private constructor with all params
    InitiativeTracker::InitiativeTracker(const AppOptions& options,
                                         const TieBreakerFunc_t& tieBreakerFunc,
                                         const PlayerRollFunc_t& playerRollFunc,
                                         const MultiPlayerRollFunc_t& multiPlayerRollFunc,
                                         std::shared_ptr<cheeto::OutputManager> out) :
      m_options(std::make_unique<AppOptions>(options)),
      m_tieBreakerFunc(tieBreakerFunc),
      m_playerRollFunc(playerRollFunc),
      m_multiPlayerRollFunc(multiPlayerRollFunc),
      m_currentTurn(0),
      m_currentRound(1),
      m_inCombat(false)
    {
      //if no output manager was provided, create one and operate in degraded logging state
      if (out == nullptr)
      {
        m_log = std::make_shared<cheeto::OutputManager>();
        m_log->addOutputLocation("stdout", std::cout, (int)m_options->verbosity, std::chrono::milliseconds(200), "\n");

        (*m_log)((int)Verbosity::WARNING) << "WARN: no output manager provided. operating in a degraded logging state\n";
      } //end  if (out == nullptr)
      else
      {
        m_log = out;
      } //end  else
      ConfigParser::m_log = m_log;

      //check functions passed in
      if (m_tieBreakerFunc == nullptr)
      {
        (*m_log)((int)Verbosity::ERROR) << "ERROR: InitiativeTracker: must provide a tiebraker function\n";
        //TODO find good way to terminate application
      } //end  if (m_tieBreakerFunc == nullptr)

      //check functions passed in
      if (m_playerRollFunc == nullptr && m_multiPlayerRollFunc == nullptr)
      {
        (*m_log)((int)Verbosity::ERROR) << "ERROR: InitiativeTracker: must provide a player roll function\n";
        //TODO find good way to terminate application
      } //end  if (m_playerRollFunc == nullptr)

      configFromSettings();
    }

    //private functions
    void InitiativeTracker::configFromSettings()
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: configuring from settings\n";

      //encounter file (if provided, don't use player or enemy file
      if (!m_options->encounterFilePath.empty())
      {
        (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: encounter file provided, loading...\n";
        addEncounterFromFile(m_options->encounterFilePath);
        return;
      } //end  if (!m_options->encounterFilePath.empty())

      //player file
      if (!m_options->playerFilePath.empty())
      {
        (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: player file provided, loading...\n";
        addPlayersFromFile(m_options->playerFilePath);
      } //end  if (!m_options->playerFilePath.empty())

      //enemy file
      if (!m_options->enemyFilePath.empty())
      {
        (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: enemy file provided, loading...\n";
        addEnemiesFromFile(m_options->enemyFilePath);
      } //end  if (!m_options->enemyFilePath.empty())
    }
    void InitiativeTracker::addEnemies(const std::vector<Creature>& enemies)
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: adding " << enemies.size() << " enemies\n";

      for (auto& enemy : enemies)
      {
        (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: adding enemy " << enemy.name << "\n";
        m_creatures[enemy.name] = enemy;
      } //end  for (auto& enemy : enemies)
    }
    void InitiativeTracker::addPlayers(const std::vector<Player>& players)
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: adding " << players.size() << " players\n";

      for (auto& player : players)
      {
        (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: adding player " << player.playerName << " : " << player.characterName << "\n";
        m_players[player.playerName] = player;
      } //end  for (auto& player : players)
    }

    //constructors
    InitiativeTracker::InitiativeTracker(const AppOptions& options,
                                         const TieBreakerFunc_t& tieBreakerFunc,
                                         const PlayerRollFunc_t& playerRollFunc,
                                         std::shared_ptr<cheeto::OutputManager> out) :
      InitiativeTracker(options, tieBreakerFunc, playerRollFunc, nullptr, out)
    {
    }
    InitiativeTracker::InitiativeTracker(const AppOptions& options,
                                         const TieBreakerFunc_t& tieBreakerFunc,
                                         const MultiPlayerRollFunc_t& multiPlayerRollFunc,
                                         std::shared_ptr<cheeto::OutputManager> out) :
      InitiativeTracker(options, tieBreakerFunc, nullptr, multiPlayerRollFunc, out)
    {
    }

    //combat order functions
    void InitiativeTracker::startCombat()
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: Starting combat. initializing state\n";
      m_currentTurn = 0;
      m_currentRound = 1;
      m_inCombat = true;

      //setup default player combatant
      Combatant newCombatant;
      newCombatant.player = true;
      newCombatant.dead = false;

      decltype(m_turnOrder) unsortedCombatants;

      //get player rolls
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: getting player rolls\n";
      if (m_playerRollFunc != nullptr)
      {
        //handle player rolls one at a time
        for (const auto& player : m_players)
        {
          newCombatant.roll = m_playerRollFunc(player.second);
          newCombatant.name = player.first;
          unsortedCombatants.push_back(newCombatant);
          (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: player "
                                               << player.second.playerName << "("
                                               << player.second.characterName << ") rolled "
                                               << newCombatant.roll << "\n";
        } //end  for (const auto& player : m_players)
      } //end  if (m_playerRollFunc != nullptr)
      else
      {
        //create vector of all players
        std::vector<Player> players;
        for (const auto& p : m_players)
        {
          players.push_back(p.second);
        } //end  for (const auto& p : m_players)

        //call callback function
        auto playerRolls = m_multiPlayerRollFunc(players);
        (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: got rolls for "
                                             << playerRolls.size() << " players\n";

        //create combatants
        Combatant c;
        c.dead = false;
        c.player = true;
        for (auto& [player, roll] : playerRolls)
        {
          c.name = player.playerName;
          c.roll = roll;

          unsortedCombatants.push_back(c);
        }
      } //end  else

      //setup default enemy combatant
      newCombatant.player = false;

      //roll for enemies
      (*m_log)((int)Verbosity::INFO) << "INFO: InitiativeTracker: rolling initiative for enemies...\n";
      for (auto& enemy : m_creatures)
      {
        //configure information based on who the enemy is
        newCombatant.name = enemy.first;
        (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: rolling for " << newCombatant.name << "\n";

        //initial roll
        newCombatant.roll = enemy.second.roller.roll();
        (*m_log)((int)Verbosity::DEBUG_INFO) << "\troll: " << newCombatant.roll << "\n";

        //advantage?
        if (enemy.second.advantage)
        {
          auto advantageRoll = std::max(newCombatant.roll, enemy.second.roller.roll());
          (*m_log)((int)Verbosity::DEBUG_INFO) << "\tadvantage roll: " << advantageRoll << "\n";
          newCombatant.roll = std::max(newCombatant.roll, advantageRoll);
        } //end  if (enemy.second.ad )

        (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: final roll: " << newCombatant.roll << "\n";
        unsortedCombatants.push_back(newCombatant);
      } //end  for (const auto& enemy : m_creatures)

      //sort the combatnats by their roll
      std::sort(unsortedCombatants.begin(), unsortedCombatants.end(), std::greater<decltype(unsortedCombatants[0])>());

      //handle ties
      decltype(m_turnOrder) tiedCombatants;
      bool newTie = true;
      int roll = 0;
      for (int i = 0; i < unsortedCombatants.size(); i++)
      {
        if (newTie)
        {
          roll = unsortedCombatants[i].roll;
          newTie = false;
        } //end  if (newTie)
        if (roll == unsortedCombatants[i].roll)
        {
          tiedCombatants.push_back(unsortedCombatants[i]);
        } //end  if (roll == unsortedCombatants[i].roll)
        else
        {
          auto sortedTies = tiedCombatants.size() == 1 ? tiedCombatants : m_tieBreakerFunc(tiedCombatants);
          m_turnOrder.insert(m_turnOrder.begin(), sortedTies.begin(), sortedTies.end());
          i--; //decrement to handle this unmatched next iteration;
          newTie = true; //setup to have new tie checking start
          tiedCombatants.clear();
        } //end  else
      } //end  for (int i = 0; i < unsortedCombatants.size(); i++)
      auto sortedTies = tiedCombatants.size() == 1 ? tiedCombatants : m_tieBreakerFunc(tiedCombatants);
      m_turnOrder.insert(m_turnOrder.begin(), sortedTies.begin(), sortedTies.end());

      //TODO figure this out
      //reverse the order because for some reason they are sorted in ascending order
      std::stable_sort(m_turnOrder.begin(), m_turnOrder.end(), std::greater<Combatant>());

      //debug print all things
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: Final initiative order:\n";
      for (const auto& combatant : m_turnOrder)
      {
        (*m_log)((int)Verbosity::VERBOSE_INFO) << "\t" << combatant.name << ": " << combatant.roll << "\n";
      } //end  for (const auto& combatant : m_turnOrder)
    }
    void InitiativeTracker::stopCombat()
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: stopping combat.\n";
      m_inCombat = false;
      m_currentRound = 0;
      m_currentTurn = 0;
      m_turnOrder.clear();
    }
    void InitiativeTracker::nextTurn()
    {
      (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: InitiativeTracker: nextTurn\n";
      m_currentTurn++;

      //check for next round
      if (m_currentTurn >= m_turnOrder.size())
      {
        (*m_log)((int)Verbosity::DEBUG_INFO) << "DEBUG: InitiativeTracker: Next Round\n";
        m_currentTurn = 0;
        m_currentRound++;
      } //end  if (m_currentTurn >= m_combatants.size())
    }
    Combatant InitiativeTracker::currentCombatant() const
    {
      (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: InitiativeTracker: currentCombatant\n";
      return m_turnOrder[m_currentTurn];
    }
    Player InitiativeTracker::onDeckPlayer() const
    {
      (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: InitiativeTracker: onDeckPlayer\n";
      bool onDeckFound = false;
      size_t count = 1;
      while (!onDeckFound && count < m_turnOrder.size())
      {
        if (m_turnOrder[(m_currentTurn + count) % m_turnOrder.size()].player)
        {
          onDeckFound = true;
        } //end  if (std::get<2>(m_combatants[(m_currentTurn + count) % m_combatants.size()]))
        else
        {
          count++;
        } //end  else
      } //end  while (!onDeckFound && count < m_combatants.size())

      if (!onDeckFound)
      {
        (*m_log)((int)Verbosity::ERROR) << "ERROR: InitiativeTracker: Couldn't find on deck player. Returning default constructed\n";
        return Player();
      } //end  if (!onDeckFound)

      //TODO put checking in here to make sure name is found
      return m_players.at(m_turnOrder[(m_currentTurn + count) % m_turnOrder.size()].name);
    }

    //combatant functions
    void InitiativeTracker::killEnemy(const std::string& name)
    {
      //TODO
    }
    void InitiativeTracker::reviveEnemy(const std::string& name)
    {
      //TODO
    }
    void InitiativeTracker::killPlayer(const std::string& name)
    {
      //TODO
    }
    void InitiativeTracker::revivePlayer(const std::string& name)
    {
      //TODO
    }

    //setup functions
    bool InitiativeTracker::addPlayersFromFile(const std::string& filePath)
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "DEBUG: InitiativeTracker: adding players from " << filePath << "\n";

      auto players = ConfigParser::parsePlayerFile(filePath);
      addPlayers(players);

      return !players.empty();
    }
    bool InitiativeTracker::addEnemiesFromFile(const std::string& filePath)
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "DEBUG: InitiativeTracker: adding enemies from " << filePath << "\n";

      auto enemies = ConfigParser::parseEnemyFile(filePath);
      addEnemies(enemies);

      return !enemies.empty();
    }
    bool InitiativeTracker::addEncounterFromFile(const std::string& filePath)
    {
      (*m_log)((int)Verbosity::VERBOSE_INFO) << "INFO: InitiativeTracker: adding encountter from " << filePath << "\n";

      //read file
      auto [enemies, players] = ConfigParser::parseEncounterFile(filePath);

      addEnemies(enemies);
      addPlayers(players);

      return !(enemies.empty() || players.empty());
    }

    //getters
    bool InitiativeTracker::getInCombat() const
    {
      return m_inCombat;
    }
    std::uint64_t InitiativeTracker::getRound() const
    {
      (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: InitiativeTracker: getRound\n";
      return m_currentRound;
    }
    std::uint64_t InitiativeTracker::getTurn() const
    {
      (*m_log)((int)Verbosity::FUNCTION_NAME) << "FNAME: InitiativeTracker: getTurn\n";
      //return +1 because turn is used as an index
      return m_currentTurn + 1;
    }
    Player InitiativeTracker::getPlayer(const std::string& name) const
    {
      const auto& playerIt = m_players.find(name);
      return (playerIt == m_players.end() ? Player() : playerIt->second);
    }
    bool InitiativeTracker::playerExists(const std::string& name) const
    {
      return m_players.count(name) > 0;
    }
    Creature InitiativeTracker::getEnemy(const std::string& name) const
    {
      const auto& enemyIt = m_creatures.find(name);
      return (enemyIt == m_creatures.end() ? Creature() : enemyIt->second);
    }
    bool InitiativeTracker::enemyExists(const std::string& name) const
    {
      return m_creatures.count(name) > 0;
    }
    std::vector<Combatant> InitiativeTracker::getTurnOrder() const
    {
      return m_turnOrder;
    }
  }
}
