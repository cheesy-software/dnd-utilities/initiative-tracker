#include <iostream>
#include <filesystem>
#include <fstream>

#include "ApplicationOptions.h"
#include "argagg.hpp"
#include "OutputManager.h"

#include "InitiativeTracker.h"

//constants
constexpr const char* LOG_NAME = "initiative_tracker.log";
constexpr const char* VERBOSITY_OPTION = "verbosity";
constexpr const char* LOG_PATH_OPTION = "logPath";
constexpr const char* HELP_OPTION = "help";
constexpr const char* ENEMY_FILE_OPTION = "creatureFile";
constexpr const char* PLAYER_FILE_OPTION = "playerFile";
constexpr const char* ENCOUNTER_FILE_OPTION = "encounterFile";

//using declarations
using CheesySoftware::InitiativeTracker::AppOptions;
using CheesySoftware::InitiativeTracker::Verbosity;
using CheesySoftware::InitiativeTracker::InitiativeTracker;
using CheesySoftware::InitiativeTracker::Combatant;

std::string handleArgs(const argagg::parser& parser, int argc, char** argv, AppOptions& options);
std::vector<Combatant> handleTie(const std::vector<Combatant>& combatants);
int getPlayerRoll(const CheesySoftware::InitiativeTracker::Player& player);

std::shared_ptr<cheeto::OutputManager> out;

int main(int argc, char** argv)
{
  AppOptions options;

  //parse args
  argagg::parser argparser {{
    {HELP_OPTION, {"-h", "--help"}, "shows this help message", 0},
    {VERBOSITY_OPTION, {"-v", "--verbosity"}, "verbosity (default: 3)", 1},
    {LOG_PATH_OPTION, {"-l", "--log-path"}, "path to put the log file (default: ./)", 1},
    {ENEMY_FILE_OPTION, {"-c", "--enemy-file"}, "path to the enemy file", 1},
    {PLAYER_FILE_OPTION, {"-p", "--player-file"}, "path to the player file", 1},
    {ENCOUNTER_FILE_OPTION, {"-e", "--encounter-file"}, "path to the encounter file", 1}
  }};
  auto argsReturnStr = handleArgs(argparser, argc, argv, options);
  if (argsReturnStr != "")
  {
    std::cerr << argsReturnStr << std::endl << std::endl;
    std::cerr << "Usage: InitiativeTracker [options]" << std::endl << argparser;
    return -1;
  }

  //setup output manager for std out
  out = std::make_shared<cheeto::OutputManager>();
  out->addOutputLocation("stdout", std::cout, (int)options.verbosity, std::chrono::milliseconds(200), "\n");

  //add log file
  const auto fullLogPath = options.logPath + "/" + LOG_NAME;
  out->addFile("logFile", fullLogPath, (int)Verbosity::ALL_INFO, std::chrono::milliseconds(200), "\n");

  (*out)((int)Verbosity::VERBOSE_INFO) << "sucessfully created log file: " << fullLogPath << "\n";

  //cmd options
  (*out)((int)Verbosity::VERBOSE_INFO) << "Cmd Options: \n"
                                       << "Verbosity: " << (int)options.verbosity << "\n"
                                       << "Log Path: " << options.logPath << "\n"
                                       << "Encounter File: " << options.encounterFilePath << "\n"
                                       << "Player File: " << options.playerFilePath << "\n"
                                       << "Enemy File: " << options.enemyFilePath << "\n";

  InitiativeTracker app(options, std::bind(handleTie, std::placeholders::_1), std::bind(getPlayerRoll, std::placeholders::_1), out);

  app.startCombat();

  //start combat
  bool stillFighting = true;
  int fighter = 0;
  do
  {
    //output current turn
    auto curName = app.currentCombatant().name;
    auto onDeckPlayer = app.onDeckPlayer();
    (*out)((int)Verbosity::NO_INFO) << "\n\nCurrent turn: " << curName << "\n";
    (*out)((int)Verbosity::NO_INFO) << "On Deck: " << onDeckPlayer.playerName << ": " << onDeckPlayer.characterName << "\n\n";
    /*(*out)((int)Verbosity::NO_INFO) << "\n\nCurrent turn: " << app.currentCombatant().name << "\n";
     *(*out)((int)Verbosity::NO_INFO) << "On Deck: " << app.onDeckPlayer().playerName << "\n\n";*/

    //see if another turn is still needed
    std::string choice;
    (*out)((int)Verbosity::NO_INFO) << "is the fight still going (y/n)?: ";
    std::cin >> choice;
    stillFighting = (choice[0] == 'y' || choice[0] == 'Y');

    //new turn
    app.nextTurn();
  } while (stillFighting);

  app.stopCombat();

  return 0;
}

std::string handleArgs(const argagg::parser& parser, int argc, char** argv, AppOptions& options)
{
  //parse args
  argagg::parser_results args;
  try
  {
    args = parser.parse(argc, argv);

    //check for help option
    if (args[HELP_OPTION])
    {
      return "print help message";
    } //end  if (args["help"])
  } //end  try
  catch (const std::exception& e)
  {
    return e.what();
  } //end  catch (const std::exception& e)

  //log file path
  options.logPath = args[LOG_PATH_OPTION].as<std::string>("./");

  //data file paths
  options.encounterFilePath = args[ENCOUNTER_FILE_OPTION].as<std::string>("");
  options.playerFilePath = args[PLAYER_FILE_OPTION].as<std::string>("");
  options.enemyFilePath = args[ENEMY_FILE_OPTION].as<std::string>("");
  if (options.encounterFilePath.empty())
  {
    if (options.playerFilePath.empty() || options.enemyFilePath.empty())
    {
      return "Must provide either encounter file or player and enemy file";
    } //end  if (options.playerFilePath.empty() && options.enemyFilePath.empty())
  } //end  if (options.encounterFilePath.empty())

  //verbosity
  auto verInt = args[VERBOSITY_OPTION].as<int>((int)Verbosity::INFO);
  verInt = verInt > (int)Verbosity::ALL_INFO ? (int)Verbosity::ALL_INFO : verInt;
  verInt = verInt < (int)Verbosity::NO_INFO ? (int)Verbosity::NO_INFO : verInt;
  options.verbosity = (Verbosity)verInt;

  return "";
}
std::vector<Combatant> handleTie(const std::vector<Combatant>& combatants)
{
  (*out)((int)Verbosity::FUNCTION_NAME) << "handleTie()\n";
  std::vector<Combatant> unsorted = combatants;
  std::vector<Combatant> sorted;
  while (!unsorted.empty())
  {
    //print prompt
    for (int i = 0; i < unsorted.size(); i++)
    {
      (*out)((int)Verbosity::NO_INFO) << i << ". " << unsorted[i].name << "\n";
    } //end  for (int i = 0; i < combatants.size(); i++)
    (*out)((int)Verbosity::NO_INFO) << "please choose person to go first: ";

    //get result
    int result;
    std::cin >> result;
    (*out)((int)Verbosity::DEBUG_INFO) << "choice: " << result << "\n";

    //apply user choise
    sorted.push_back(unsorted[result]);
    unsorted.erase(unsorted.begin() + result);
  } //end  while (!combatants.empty())
  for (auto player : sorted)
  {
    (*out)((int)Verbosity::DEBUG_INFO) << player.name << ": " << player.roll << "\n";
  } //end  for (auto player : combatants)

  (*out)((int)Verbosity::FUNCTION_NAME) << "exiting handleTie()\n";
  return sorted;
}
int getPlayerRoll(const CheesySoftware::InitiativeTracker::Player& player)
{
  (*out)((int)Verbosity::FUNCTION_NAME) << "Main: getting roll for player: " << player.playerName << " : " << player.characterName << "\n";
  (*out)((int)Verbosity::NO_INFO) << "Enter roll for " << player.characterName << "(" << player.playerName << "): ";
  int result;
  std::cin >> result;
  (*out)((int)Verbosity::DEBUG_INFO) << "choice: " << result << "\n";
  return result;
}
