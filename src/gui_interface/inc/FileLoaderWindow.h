#ifndef CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_FILE_LOADER_WINDOW_H
#define CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_FILE_LOADER_WINDOW_H

//std includes
#include <memory>

//Qt includes
#include <QDialog>

//UI includes
#include "ui_FileLoaderWindow.h"

//custom includes
#include "OutputManager.h"

class FileLoader : public QDialog
{
  Q_OBJECT

  public:
    //public types
    enum class FileType
    {
      ENCOUNTER_FILE,
      ENEMY_FILE,
      PLAYER_FILE
    };

  private:
    //member data
    std::unique_ptr<Ui_FileLoader> m_ui;
    std::shared_ptr<cheeto::OutputManager> m_log;
    FileType m_fileType;

    //private functions
    void makeConnections();

  public:
    //constructors
    FileLoader(FileType type, std::shared_ptr<cheeto::OutputManager> log, QWidget* parent = nullptr);

  signals:
    void filesSelected(std::vector<std::string>);

  public slots:
    void btnAddFileHandler();
    void btnRemoveFileHandler();
    void btnSubmitHandler();

};

std::ostream& operator<<(std::ostream& out, FileLoader::FileType toPrint);

#endif
