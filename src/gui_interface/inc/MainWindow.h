#ifndef CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_INTERFACE_MAIN_WINDOW_H
#define CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_INTERFACE_MAIN_WINDOW_H

//std includes
#include <memory>

//qt includes
#include <QMainWindow>
#include <QMessageBox>
#include <QCoreApplication>

//custom ui includes
#include "ApplicationOptions.h"
#include "ui_MainWindow.h"

//custom includes
#include "InitiativeTracker.h"
#include "FileLoaderWindow.h" //included for filetype enum use in the header

namespace IT = CheesySoftware::InitiativeTracker;

class MainWindow : public QMainWindow
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_MainWindow> m_ui;
    std::unique_ptr<IT::InitiativeTracker> m_app;
    std::shared_ptr<cheeto::OutputManager> m_log;

    //private functions
    void makeConnections();
    std::vector<IT::Combatant> handleTie(const std::vector<IT::Combatant>& combatants);
    std::vector<std::pair<IT::Player, int>> getPlayerRolls(const std::vector<IT::Player>& players);
    void updateCombatState();
    void exitProgram();
    void updateGui(bool clear = false);
    void addFile(FileLoader::FileType type);

  public:
    //constructors
    MainWindow(const IT::AppOptions& options, QWidget* parent = nullptr);
    ~MainWindow();

  signals:

  public slots:
    //GUI button handlers
    void btnNextTurnHandler();
    void btnStartStopHandler();

    //GUI menu handlers
    void actLoadEncounterFileHandler();
    void actLoadPlayerFileHandler();
    void actLoadEnemyFileHandler();
    void actStartStopHandler();
    void actSettingsHandler();
    void actAboutHandler();
    void actAboutQtHandler();
    void actCloseHandler();
};

#endif
