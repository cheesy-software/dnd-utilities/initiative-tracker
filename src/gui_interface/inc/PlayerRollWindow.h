#ifndef CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_PLAYER_ROLL_WINDOW_H
#define CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_PLAYER_ROLL_WINDOW_H

//std includes
#include <memory>

//Qt includes
#include <QSpinBox>
#include <QLabel>
#include <QDialog>

//GUI include
#include "ui_PlayerRollWindow.h"

//custom includes
#include "InitiativeTracker.h"

namespace IT = CheesySoftware::InitiativeTracker;

class PlayerRollWindow : public QDialog
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_PlayerRollWindow> m_ui;
    std::shared_ptr<cheeto::OutputManager> m_log;
    std::vector<IT::Player> m_players;

    //constants
    static const QFont M_LABEL_FONT;

    //private functions
    void makeConnections();
    void addFormRow(const IT::Player& player);

  public:
    //constructors
    PlayerRollWindow(std::vector<IT::Player> players,
                     std::shared_ptr<cheeto::OutputManager> log,
                     QWidget* parent = nullptr);

  signals:
    void rollsEntered(const std::vector<std::pair<IT::Player, int>>& playerRolls);

  public slots:
    void btnSubmitClickedHandler();

};

#endif
