#ifndef CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_TIEBREAK_WINDOW_H
#define CHEESY_SOFTWARE_INITIATIVE_TRACKER_GUI_TIEBREAK_WINDOW_H

//UI include
#include "ui_TiebreakWindow.h"

//std includes
#include <memory>

//Qt includes
#include <QDialog>

//personal includes
#include "InitiativeTracker.h"

namespace IT = CheesySoftware::InitiativeTracker;

class TiebreakWindow : public QDialog
{
  Q_OBJECT

  private:
    std::unique_ptr<Ui_TiebreakWindow> m_ui;
    std::shared_ptr<cheeto::OutputManager> m_log;
    std::unordered_map<std::string, IT::Combatant> m_combatants;

    //private functions
    void makeConnections();
    void addListItem(const IT::Combatant& combatant);

  public:
    //constructors
    TiebreakWindow(const std::vector<IT::Combatant>& tiedCombatants,
                   std::shared_ptr<cheeto::OutputManager> log,
                   QWidget* parent = nullptr);

  signals:
    void tieResolved(const std::vector<IT::Combatant>&);

  public slots:
    void btnSubmitClickedHandler();
};

#endif
