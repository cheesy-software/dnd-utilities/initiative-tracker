//header include
#include "FileLoaderWindow.h"

//Qt includes
#include <QtAlgorithms>
#include <QFileDialog>

//custom includes
#include "ApplicationOptions.h"

namespace IT = CheesySoftware::InitiativeTracker;

//private functions
void FileLoader::makeConnections()
{
  connect(m_ui->btnSubmit, &QPushButton::clicked, this, &FileLoader::btnSubmitHandler);
  connect(m_ui->btnAddFile, &QPushButton::clicked, this, &FileLoader::btnAddFileHandler);
  connect(m_ui->btnRemoveFile, &QPushButton::clicked, this, &FileLoader::btnRemoveFileHandler);
}

//constructors
FileLoader::FileLoader(FileType type, std::shared_ptr<cheeto::OutputManager> log, QWidget* parent) :
  QDialog(parent),
  m_ui(std::make_unique<Ui_FileLoader>()),
  m_log(log),
  m_fileType(type)
{
  //setup UI
  m_ui->setupUi(this);

  //if no output manager was provided, create one and operate in degraded logging state
  if (m_log == nullptr)
  {
    m_log = std::make_shared<cheeto::OutputManager>();
    m_log->addOutputLocation("stdout", std::cout, (int)IT::Verbosity::INFO, std::chrono::milliseconds(200), "\n");

    (*m_log)((int)IT::Verbosity::WARNING) << "WARN: FileLoader: no output manager provided. operating in a degraded logging state\n";
  } //end  if (m_log == nullptr)

  makeConnections();
}

//public slots
void FileLoader::btnAddFileHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: FileLoader: btnAddFileHandler()\n";
  auto files = QFileDialog::getOpenFileNames(this, "Select files to open", "./", "JSON (*.json)");

  //add files to list
  m_ui->lstFiles->addItems(files);
}
void FileLoader::btnRemoveFileHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: FileLoader: btnRemoveFileHandler()\n";
  qDeleteAll(m_ui->lstFiles->selectedItems());
}
void FileLoader::btnSubmitHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: FileLoader: btnSubmitHandler()\n";

  //get files
  std::vector<std::string> files;
  for (int i = 0; i < m_ui->lstFiles->count(); i++)
  {
    auto file = m_ui->lstFiles->item(i)->text().toStdString();
    (*m_log)((int)IT::Verbosity::DEBUG_INFO) << "adding file \'" << file << "\' to be loaded\n";
    files.push_back(file);
  } //end  for (int i = 0; i < m_ui->lstFiles->count(); i++)

  //emit that files have been chosen
  emit filesSelected(files);

  //close window
  close();
}

std::ostream& operator<<(std::ostream& out, FileLoader::FileType toPrint)
{
  switch(toPrint)
  {
    case FileLoader::FileType::ENCOUNTER_FILE:
      out << "Encouter File";
      break;
    case FileLoader::FileType::ENEMY_FILE:
      out << "Enemy File";
      break;
    case FileLoader::FileType::PLAYER_FILE:
      out << "Player File";
      break;
    default:
      out << "INVALID";
      break;
  }

  return out;
}

