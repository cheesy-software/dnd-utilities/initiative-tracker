//header include
#include "MainWindow.h"

//std includes
#include <functional>
#include <iostream>
#include <memory>

//window includes
#include "PlayerRollWindow.h"
#include "TiebreakWindow.h"

//custom includes
#include "ApplicationOptions.h"

using CheesySoftware::InitiativeTracker::InitiativeTracker;

//private functions
void MainWindow::makeConnections()
{
  //buttons
  connect(m_ui->btnNextTurn, &QPushButton::clicked, this, &MainWindow::btnNextTurnHandler);
  connect(m_ui->btnStartStop, &QPushButton::clicked, this, &MainWindow::btnStartStopHandler);

  //menu actions
  connect(m_ui->actAbout, &QAction::triggered, this, &MainWindow::actAboutHandler);
  connect(m_ui->actAboutQt, &QAction::triggered, this, &MainWindow::actAboutQtHandler);
  connect(m_ui->actSettings, &QAction::triggered, this, &MainWindow::actSettingsHandler);
  connect(m_ui->actLoadEnemyFile, &QAction::triggered, this, &MainWindow::actLoadEnemyFileHandler);
  connect(m_ui->actLoadPlayerFile, &QAction::triggered, this, &MainWindow::actLoadPlayerFileHandler);
  connect(m_ui->actLoadEncounterFile, &QAction::triggered, this, &MainWindow::actLoadEncounterFileHandler);
  connect(m_ui->actStartStopFight, &QAction::triggered, this, &MainWindow::actStartStopHandler);
  connect(m_ui->actClose, &QAction::triggered, this, &MainWindow::actCloseHandler);
}
std::vector<IT::Combatant> MainWindow::handleTie(const std::vector<IT::Combatant>& combatants)
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: handleTie()\n";
  std::vector<IT::Combatant> toReturn;

  //create the window
  auto window = new TiebreakWindow(combatants, m_log, this);
  connect(window, &TiebreakWindow::tieResolved, [&toReturn](const std::vector<IT::Combatant>& sortedCombatants) {
      toReturn = sortedCombatants;
    });
  window->exec();

  return toReturn;
}
std::vector<std::pair<IT::Player, int>> MainWindow::getPlayerRolls(const std::vector<IT::Player>& players)
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: getPlayerRolls()\n";
  std::vector<std::pair<IT::Player, int>> toReturn;

  //create the window
  auto window = new PlayerRollWindow(players, m_log, this);
  connect(window, &PlayerRollWindow::rollsEntered, [&toReturn](const std::vector<std::pair<IT::Player, int>>& newCombatants) {
      toReturn = newCombatants;
    });
  window->exec();

  return toReturn;
}
void MainWindow::updateCombatState()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: updateCombatState()\n";
  if (m_app->getInCombat())
  {
    (*m_log)((int)IT::Verbosity::VERBOSE_INFO) << "INFO: stoping combat\n";
    m_app->stopCombat();

    //update UI text
    m_ui->btnStartStop->setText("Start Combat");
    m_ui->actStartStopFight->setText("Start Combat");
    updateGui(true);

    //clear list
    m_ui->lstOrder->clear();
  } //end  if (m_app->getInCombat())
  else
  {
    (*m_log)((int)IT::Verbosity::VERBOSE_INFO) << "INFO: starting combat\n";
    m_app->startCombat();

    //update UI text
    m_ui->btnStartStop->setText("Stop Combat");
    m_ui->actStartStopFight->setText("Stop Combat");

    //set list
    auto turnOrder = m_app->getTurnOrder();
    for (const auto& c : turnOrder)
    {
      m_ui->lstOrder->addItem(QString::fromStdString(c.name));
    } //end  for (const auto& c : turnOrder)

    updateGui();
  } //end  else
}
void MainWindow::exitProgram()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: exitProgram()\n";

  //TODO close logic (if any needed)
  close();
}
void MainWindow::updateGui(bool clear)
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: updateGui(" << clear << ")\n";
  if (!clear)
  {
    //get the on deck player and construct string for the label
    auto p = m_app->onDeckPlayer();
    QString onDeckText = QString::fromStdString(p.characterName) + "(" +
                         QString::fromStdString(p.playerName) + ")";
    m_ui->lblOnDeck->setText(onDeckText);

    //get the current combatant and construct string for the label
    auto c = m_app->currentCombatant();
    QString currentText = QString::fromStdString(c.name);
    m_ui->lblCurrentPlayer->setText(currentText);

    //update the current highlighted item in the list
    m_ui->lstOrder->setCurrentRow(0, QItemSelectionModel::Clear);
    m_ui->lstOrder->setCurrentRow(m_app->getTurn() - 1, QItemSelectionModel::Select);
  } //end  if (!clear)
  else
  {
    m_ui->lblCurrentPlayer->clear();
    m_ui->lblOnDeck->clear();
    m_ui->lstOrder->setCurrentRow(0, QItemSelectionModel::Clear);
  } //end  else
}
void MainWindow::addFile(FileLoader::FileType type)
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: addFile(" << type << ")\n";
  std::vector<std::string> filesToLoad;

  //open window to get files to load
  auto window = new FileLoader(type, m_log, this);
  connect(window, &FileLoader::filesSelected, [&filesToLoad](const std::vector<std::string>& files) {
      filesToLoad = files;
    });
  window->exec();

  //load files
  (*m_log)((int)IT::Verbosity::DEBUG_INFO) << "DEBUG: MainWindow: adding " << filesToLoad.size() << " files\n";
  for (const auto& file : filesToLoad)
  {
    (*m_log)((int)IT::Verbosity::VERBOSE_INFO) << "INFO: MainWindow: loading \'" << file << "\'\n";
    bool fileLoaded = false;
    switch(type)
    {
      case FileLoader::FileType::ENCOUNTER_FILE:
        fileLoaded = m_app->addEncounterFromFile(file);
        break;
      case FileLoader::FileType::ENEMY_FILE:
        fileLoaded = m_app->addEnemiesFromFile(file);
        break;
      case FileLoader::FileType::PLAYER_FILE:
        fileLoaded = m_app->addPlayersFromFile(file);
        break;
    } //end  switch(type)

    if (!fileLoaded)
    {
      (*m_log)((int)IT::Verbosity::ERROR) << "ERROR: MainWindow: Failed to load \'" << file << "\'\n";
    } //end  if (!fileLoaded)
  } //end  for (const auto& file : filesToLoad)
}

MainWindow::MainWindow(const IT::AppOptions& options, QWidget* parent) :
  QMainWindow(parent),
  m_log(std::make_shared<cheeto::OutputManager>())
{
  //setup UI
  m_ui = std::make_unique<Ui_MainWindow>();
  m_ui->setupUi(this);

  makeConnections();

  //setup output manager
  m_log->addOutputLocation("stdout", std::cout, (int)options.verbosity, std::chrono::milliseconds(200), "\n");
  m_log->addFile("logFile", options.logPath, (int)IT::Verbosity::ALL_INFO, std::chrono::milliseconds(200), "\n");

  m_app = std::make_unique<IT::InitiativeTracker>(options,
                                                  //std::bind(this, &MainWindow::handleTie, std::placeholders::_1),
                                                  [this] (const std::vector<IT::Combatant>& combatants) {
                                                    return handleTie(combatants);
                                                  },
                                                  //std::bind(this, &MainWindow::getPlayerRoll, std::placeholders::_1),
                                                  [this] (const std::vector<IT::Player>& players) {
                                                    return getPlayerRolls(players);
                                                  },
                                                  m_log);
}

MainWindow::~MainWindow()
{
}

//GUI button handlers
void MainWindow::btnNextTurnHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: btnNextTurnHandler()\n";
  if (m_app->getInCombat())
  {
    //start the next turn
    m_app->nextTurn();

    //update labels
    updateGui();
  } //end  if (m_app->getInCombat())
  else
  {
    (*m_log)((int)IT::Verbosity::INFO) << "INFO: MainWindow: Not currently in combat.\n";
  } //end  else
}
void MainWindow::btnStartStopHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: btnStartStopHandler()\n";
  updateCombatState();
}

//GUI menu handlers
void MainWindow::actLoadEncounterFileHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actLoadEncounterFileHandler()\n";
  addFile(FileLoader::FileType::ENCOUNTER_FILE);
}
void MainWindow::actLoadPlayerFileHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actLoadPlayerFileHandler()\n";
  addFile(FileLoader::FileType::PLAYER_FILE);
}
void MainWindow::actLoadEnemyFileHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actLoadEnemyFileHandler\n";
  addFile(FileLoader::FileType::ENEMY_FILE);
}
void MainWindow::actStartStopHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actStartStophandler()\n";
  updateCombatState();
}
void MainWindow::actSettingsHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actSettingsHandler()\n";
  //TODO
}
void MainWindow::actAboutHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actAboutHandler()\n";
  QMessageBox msgBox;
  msgBox.setText(QCoreApplication::applicationName());
  msgBox.setInformativeText("version: " + QCoreApplication::applicationVersion() + "\n"
                            "Author: " + QCoreApplication::organizationName() + "\n"
                           );
  msgBox.exec();
}
void MainWindow::actAboutQtHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actAboutQtHandler()\n";
  QMessageBox::aboutQt(this);
}
void MainWindow::actCloseHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: MainWindow: actCloseHandler()\n";
  exitProgram();
}
