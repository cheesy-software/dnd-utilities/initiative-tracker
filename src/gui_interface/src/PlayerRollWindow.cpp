#include "PlayerRollWindow.h"

//static constant initialization
const QFont PlayerRollWindow::M_LABEL_FONT = QFont("Times", 14);

//private functions
void PlayerRollWindow::makeConnections()
{
  connect(m_ui->btnSubmit, &QPushButton::clicked, this, &PlayerRollWindow::btnSubmitClickedHandler);
}
void PlayerRollWindow::addFormRow(const IT::Player& player)
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: PlayerRollWindow: addFormRow\n";
  auto lbl = new QLabel(QString::fromStdString(player.toString()), this);
  lbl->setFont(M_LABEL_FONT);

  QSpinBox* spnBox = new QSpinBox(this);
  spnBox->setObjectName(lbl->text()); //TODO fix
  //spnBox->setObjectName(QString::fromStdString(player.characterName));
  spnBox->setMinimum(-100);
  spnBox->setFont(M_LABEL_FONT);

  m_ui->layPlayers->addRow(lbl, spnBox);
}

//constructors
PlayerRollWindow::PlayerRollWindow(std::vector<IT::Player> players,
                                   std::shared_ptr<cheeto::OutputManager> log,
                                   QWidget* parent) :
  QDialog(parent),
  m_log(log),
  m_players(players)
{
  //setup UI
  m_ui = std::make_unique<Ui_PlayerRollWindow>();
  m_ui->setupUi(this);

  //connect UI elements
  makeConnections();

  //if no output manager was provided, create one and operate in degraded logging state
  if (m_log == nullptr)
  {
    m_log = std::make_shared<cheeto::OutputManager>();
    m_log->addOutputLocation("stdout", std::cout, (int)IT::Verbosity::INFO, std::chrono::milliseconds(200), "\n");

    (*m_log)((int)IT::Verbosity::WARNING) << "WARN: PlayerRollWindow: no output manager provided. operating in a degraded logging state\n";
  } //end  if (m_log == nullptr)

  //make form rows
  for (const auto& player : m_players)
  {
    (*m_log)((int)IT::Verbosity::DEBUG_INFO) << "DEBUG: PlayerRollWindow: adding form row for "
                                             << player << "\n";
    addFormRow(player);
  } //end  for (const auto& player : players)
}

//public slots
void PlayerRollWindow::btnSubmitClickedHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: PlayerRollWindow: submit button clicked\n";

  //generate combatant vector
  std::vector<std::pair<IT::Player, int>> playerRolls;
  //auto spinBoxes = findChildren<QSpinBox*>();
  //for (auto box : spinBoxes)
  for (const auto& player : m_players)
  {
    //c.name = box->objectName().toStdString();
    //c.roll = box->value();
    auto box = this->findChild<QSpinBox*>(QString::fromStdString(player.toString()));
    int roll = 0;
    if (box == nullptr)
    {
      (*m_log)((int)IT::Verbosity::ERROR) << "ERROR: PlayerRollWindow: no spinbox found for " << player << "\n"
                                          << "\tUsing 0 for the player's roll\n";
    } //end  if (box == nullptr)
    else
    {
      roll = box->value();
    } //end  else

    (*m_log)((int)IT::Verbosity::DEBUG_INFO) << "DEBUG: PlayerRollWindow: " << player << ": " << roll << "\n";
    playerRolls.push_back({player, roll});
  } //end  for (const auto& player : m_players)

  //emit the rolls
  emit rollsEntered(playerRolls);

  //close dialog
  close();
}
