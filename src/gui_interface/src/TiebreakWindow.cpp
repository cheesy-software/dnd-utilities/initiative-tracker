#include "TiebreakWindow.h"

//private functions
void TiebreakWindow::makeConnections()
{
  connect(m_ui->btnSubmit, &QPushButton::clicked, this, &TiebreakWindow::btnSubmitClickedHandler);
}
void TiebreakWindow::addListItem(const IT::Combatant& combatant)
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: TiebreakWindow: addListItem()\n";

  //create the list widget item
  auto item = new QListWidgetItem(QString::fromStdString(combatant.name));

  //add item to the list
  m_ui->lstCombatants->addItem(item);
}

//constructors
TiebreakWindow::TiebreakWindow(const std::vector<IT::Combatant>& tiedCombatants,
               std::shared_ptr<cheeto::OutputManager> log,
               QWidget* parent) :
  QDialog(parent),
  m_ui(std::make_unique<Ui_TiebreakWindow>()),
  m_log(log)
{
  //setup UI
  m_ui->setupUi(this);

  //connect UI elements
  makeConnections();

  //if no output manager was provided, create one and operate in degraded logging state
  if (m_log == nullptr)
  {
    m_log = std::make_shared<cheeto::OutputManager>();
    m_log->addOutputLocation("stdout", std::cout, (int)IT::Verbosity::INFO, std::chrono::milliseconds(200), "\n");

    (*m_log)((int)IT::Verbosity::WARNING) << "WARN: TiebreakWindow: no output manager provided. operating in a degraded logging state\n";
  } //end  if (m_log == nullptr)

  //make form rows and setup map
  for (const auto& c : tiedCombatants)
  {
    (*m_log)((int)IT::Verbosity::DEBUG_INFO) << "DEBUG: TiebreakWindow: adding list item for " << c.name << "\n";
    addListItem(c);

    //add combatant to a map for later sorting
    m_combatants[c.name] = c;
  } //end  for (const auto& player : players)
}

//public slots
void TiebreakWindow::btnSubmitClickedHandler()
{
  (*m_log)((int)IT::Verbosity::FUNCTION_NAME) << "FNAME: TiebreakWindow: submit button clicked\n";
  std::vector<IT::Combatant> untiedCombatants;

  //iterate over items and add combatants to a sorted list
  for (int i = 0; i < m_ui->lstCombatants->count(); i++)
  {
    //get the combatant pointed to by this list item
    auto item = m_ui->lstCombatants->item(i);
    auto c = m_combatants[item->text().toStdString()];

    //insert it into the vector
    (*m_log)((int)IT::Verbosity::DEBUG_INFO) << "DEBUG: TiebreakWindow: " << i << ". " << c.name << "\n";
    untiedCombatants.push_back(c);
  } //end  for (int i = 0; i < m_ui->lstCombatants->count(); i++)

  //emit signal indicating the tie has been resolved
  emit tieResolved(untiedCombatants);

  //close window
  close();
}

