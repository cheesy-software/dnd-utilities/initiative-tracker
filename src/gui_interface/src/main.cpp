#include <filesystem>

#include <qapplication.h>
#include <qcommandlineparser.h>

#include "MainWindow.h"

using CheesySoftware::InitiativeTracker::AppOptions;
using CheesySoftware::InitiativeTracker::Verbosity;

const QCommandLineOption verbosityOption = {{"v", "verbosity"}, "Verbosity of the info displayed", "verbosityLevel", "3"};
const QCommandLineOption logPathOption = {{"l", "log-path"}, "path to put the log file", "logPath", "./"};
const QCommandLineOption enemyFileOption = {{"c", "enemy-file"}, "path to the enemy file", "enemyFile"};
const QCommandLineOption playerFileOption = {{"p", "player-file"}, "path to the player file", "playerFile"};
const QCommandLineOption encounterFileOption = {{"e", "encounter-file"}, "path to the encounter file", "encounterFile"};

bool parseCmd(QCommandLineParser& parser, AppOptions& options);

int main(int argc, char** argv)
{
  QApplication app(argc, argv);

  //setup application information
  QCoreApplication::setApplicationName("Initiative Tracker");
  QCoreApplication::setApplicationVersion("1.0.2");
  QCoreApplication::setOrganizationName("Cheesy Software");

  //parse cmd args
  QCommandLineParser parser;
  AppOptions options;
  if (!parseCmd(parser, options))
  {
    parser.showHelp(1);
  }

  //create and show main window
  MainWindow mainWindow(options);
  mainWindow.show();

  return app.exec();
}

bool parseCmd(QCommandLineParser& parser, AppOptions& options)
{
  parser.addHelpOption();
  parser.setApplicationDescription("Initiative tracker for D&D");

  parser.addOptions({verbosityOption, logPathOption, enemyFileOption,
                     playerFileOption, encounterFileOption});

  parser.process(QCoreApplication::arguments());

  bool conversionSuccess = false;

  //verbosity
  int verbosity = parser.value(verbosityOption).toInt(&conversionSuccess);
  if (!conversionSuccess)
  {
    return false;
  }
  verbosity = std::max((int)Verbosity::NO_INFO, verbosity);
  verbosity = std::min((int)Verbosity::ALL_INFO, verbosity);
  options.verbosity = (Verbosity)verbosity;

  //resource files
  options.encounterFilePath = parser.value(encounterFileOption).toStdString();
  options.playerFilePath = parser.value(playerFileOption).toStdString();
  options.enemyFilePath = parser.value(enemyFileOption).toStdString();

  //log file
  options.logPath = parser.value(logPathOption).toStdString();
  if (options.logPath.empty())
  {
    options.logPath = "./initiative_tracker.log";
  }
  else
  {
    options.logPath += "/initiative_tracker.log";
  }

  return true;
}
